import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './views/header/header.component';
import { SidebarComponent } from './views/sidebar/sidebar.component';
import { HomeComponent } from './views/home/home.component';
import { FooterComponent } from './views/footer/footer.component';
import { CountriesComponent } from './views/countries/countries.component';
import { DirectorsComponent } from './views/directors/directors.component';
import { GendersComponent } from './views/genders/genders.component';
import { ActorsComponent } from './views/actors/actors.component';
import { BillboardsComponent } from './views/billboards/billboards.component';
import { ToastrModule } from 'ngx-toastr';
import { HttpClientModule } from '@angular/common/http';
import { FormCountriesComponent } from './views/countries/form/form-countries/form-countries.component';
import { ListCountriesComponent } from './views/countries/list/list-countries/list-countries.component';
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ListGendersComponent } from './views/genders/list/list-genders/list-genders.component';
import { FormGendersComponent } from './views/genders/form/form-genders/form-genders.component';
import { ListActorsComponent } from './views/actors/list/list-actors/list-actors.component';
import { FormActorsComponent } from './views/actors/form/form-actors/form-actors.component';
import { FormDirectorsComponent } from './views/directors/form/form-directors/form-directors.component';
import { ListDirectorsComponent } from './views/directors/list/list-directors/list-directors.component';
import { FormBillboardsComponent } from './views/billboards/form/form-billboards/form-billboards.component';
import { ListBillboardsComponent } from './views/billboards/list/list-billboards/list-billboards.component';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    SidebarComponent,
    HomeComponent,
    FooterComponent,
    CountriesComponent,
    DirectorsComponent,
    GendersComponent,
    ActorsComponent,
    BillboardsComponent,
    FormCountriesComponent,
    ListCountriesComponent,
    ListGendersComponent,
    FormGendersComponent,
    ListActorsComponent,
    FormActorsComponent,
    FormDirectorsComponent,
    ListDirectorsComponent,
    FormBillboardsComponent,
    ListBillboardsComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ToastrModule.forRoot(),
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    BrowserAnimationsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
