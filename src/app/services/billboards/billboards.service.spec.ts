import { TestBed } from '@angular/core/testing';

import { BillboardsService } from './billboards.service';

describe('BillboardsService', () => {
  let service: BillboardsService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(BillboardsService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
