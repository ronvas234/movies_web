import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { BehaviorSubject, Observable } from 'rxjs';
import { Billboards, BillboardsRequest } from 'src/app/models/billboards';
import { environment } from 'src/environments/environment';
import { Response } from 'src/app/models/response';

@Injectable({
  providedIn: 'root'
})
export class BillboardsService {
  public lts: Billboards[] | undefined;
  response: Response | undefined;
  url: string=`${environment.apiUrl}/api/Billboards`;
  
  constructor(private _httpcli: HttpClient,
    private toastr: ToastrService) { }

    private updateform= new BehaviorSubject<Billboards>({} as any);
    get(){
      return this._httpcli.get(this.url).subscribe(response => {
        this.response=response as Response;
        this.lts = this.response?.data as Billboards[];
        if(typeof this.response?.errors! == "object") {
          Object.values(this.response?.errors).forEach((message : any) => {
            this.toastr.error(message,"Billboards");
          });
        }
      });
    }
    getdata(): Observable<Billboards>{
      return this.updateform.asObservable();
    }
    save(request:BillboardsRequest): Observable<Response>{
      return this._httpcli.post<Response>(this.url,request);
    }
    update(request:Billboards){
      this.updateform.next(request);
    }
    updatedata(request:BillboardsRequest,id?: number): Observable<Response>{
      return this._httpcli.put<Response>(this.url+"/"+id,request);
    }
    delete(id?:number):Observable<Response>{
      return this._httpcli.delete<Response>(this.url+"/"+id);
    }
}
