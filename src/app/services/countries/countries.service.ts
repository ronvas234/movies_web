import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { BehaviorSubject, Observable } from 'rxjs';
import { Countries, CountriesRequest } from 'src/app/models/countries';
import { environment } from 'src/environments/environment';
import { Response } from 'src/app/models/response';

@Injectable({
  providedIn: 'root'
})
export class CountriesService {
  public lts: Countries[] | undefined;
  response: Response | undefined;
  url: string=`${environment.apiUrl}/api/Countries`;
  
  constructor(private _httpcli: HttpClient,
    private toastr: ToastrService) { }

    private updateform= new BehaviorSubject<Countries>({} as any);
    get(){
      return this._httpcli.get(this.url).subscribe(response => {
        this.response=response as Response;
        this.lts = this.response?.data as Countries[];
        if(typeof this.response?.errors! == "object") {
          Object.values(this.response?.errors).forEach((message : any) => {
            this.toastr.error(message,"Countries");
          });
        }
      });
    }
    getdata(): Observable<Countries>{
      return this.updateform.asObservable();
    }
    save(request:CountriesRequest): Observable<Response>{
      return this._httpcli.post<Response>(this.url,request);
    }
    update(request:Countries){
      this.updateform.next(request);
    }
    updatedata(request:CountriesRequest,id?: number): Observable<Response>{
      return this._httpcli.put<Response>(this.url+"/"+id,request);
    }
    delete(id?:number):Observable<Response>{
      return this._httpcli.delete<Response>(this.url+"/"+id);
    }
}
