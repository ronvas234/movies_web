import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { BehaviorSubject, Observable } from 'rxjs';
import { Genders, GendersRequest } from 'src/app/models/genders';
import { environment } from 'src/environments/environment';
import { Response } from 'src/app/models/response';

@Injectable({
  providedIn: 'root'
})
export class GendersService {
  public lts: Genders[] | undefined;
  response: Response | undefined;
  url: string=`${environment.apiUrl}/api/Genders`;
  
  constructor(private _httpcli: HttpClient,
    private toastr: ToastrService) { }

    private updateform= new BehaviorSubject<Genders>({} as any);
    get(){
      return this._httpcli.get(this.url).subscribe(response => {
        this.response=response as Response;
        this.lts = this.response?.data as Genders[];
        if(typeof this.response?.errors! == "object") {
          Object.values(this.response?.errors).forEach((message : any) => {
            this.toastr.error(message,"Genders");
          });
        }
      });
    }
    getdata(): Observable<Genders>{
      return this.updateform.asObservable();
    }
    save(request:GendersRequest): Observable<Response>{
      return this._httpcli.post<Response>(this.url,request);
    }
    update(request:Genders){
      this.updateform.next(request);
    }
    updatedata(request:GendersRequest,id?: number): Observable<Response>{
      return this._httpcli.put<Response>(this.url+"/"+id,request);
    }
    delete(id?:number):Observable<Response>{
      return this._httpcli.delete<Response>(this.url+"/"+id);
    }
}
