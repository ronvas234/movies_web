import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { BehaviorSubject, Observable } from 'rxjs';
import { Directors, DirectorsRequest } from 'src/app/models/directors';
import { environment } from 'src/environments/environment';
import { Response } from 'src/app/models/response';

@Injectable({
  providedIn: 'root'
})
export class DirectorsService {
  public lts: Directors[] | undefined;
  response: Response | undefined;
  url: string=`${environment.apiUrl}/api/Directors`;
  
  constructor(private _httpcli: HttpClient,
    private toastr: ToastrService) { }

    private updateform= new BehaviorSubject<Directors>({} as any);
    get(){
      return this._httpcli.get(this.url).subscribe(response => {
        this.response=response as Response;
        this.lts = this.response?.data as Directors[];
        if(typeof this.response?.errors! == "object") {
          Object.values(this.response?.errors).forEach((message : any) => {
            this.toastr.error(message,"Directors");
          });
        }
      });
    }
    getdata(): Observable<Directors>{
      return this.updateform.asObservable();
    }
    save(request:DirectorsRequest): Observable<Response>{
      return this._httpcli.post<Response>(this.url,request);
    }
    update(request:Directors){
      this.updateform.next(request);
    }
    updatedata(request:DirectorsRequest,id?: number): Observable<Response>{
      return this._httpcli.put<Response>(this.url+"/"+id,request);
    }
    delete(id?:number):Observable<Response>{
      return this._httpcli.delete<Response>(this.url+"/"+id);
    }
}
