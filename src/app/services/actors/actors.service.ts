import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { BehaviorSubject, Observable } from 'rxjs';
import { Actors, ActorsRequest } from 'src/app/models/actors';
import { environment } from 'src/environments/environment';
import { Response } from 'src/app/models/response';

@Injectable({
  providedIn: 'root'
})
export class ActorsService {
  public lts: Actors[] | undefined;
  response: Response | undefined;
  url: string=`${environment.apiUrl}/api/Actors`;
  
  constructor(private _httpcli: HttpClient,
    private toastr: ToastrService) { }

    private updateform= new BehaviorSubject<Actors>({} as any);
    get(){
      return this._httpcli.get(this.url).subscribe(response => {
        this.response=response as Response;
        this.lts = this.response?.data as Actors[];
        if(typeof this.response?.errors! == "object") {
          Object.values(this.response?.errors).forEach((message : any) => {
            this.toastr.error(message,"Actors");
          });
        }
      });
    }
    getdata(): Observable<Actors>{
      return this.updateform.asObservable();
    }
    save(request:ActorsRequest): Observable<Response>{
      return this._httpcli.post<Response>(this.url,request);
    }
    update(request:Actors){
      this.updateform.next(request);
    }
    updatedata(request:ActorsRequest,id?: number): Observable<Response>{
      return this._httpcli.put<Response>(this.url+"/"+id,request);
    }
    delete(id?:number):Observable<Response>{
      return this._httpcli.delete<Response>(this.url+"/"+id);
    }
}
