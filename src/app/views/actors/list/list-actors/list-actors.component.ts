import { Component, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { Actors } from 'src/app/models/actors';
import { ActorsService } from 'src/app/services/actors/actors.service';
import { CountriesService } from 'src/app/services/countries/countries.service';

@Component({
  selector: 'app-list-actors',
  templateUrl: './list-actors.component.html',
  styleUrls: ['./list-actors.component.scss']
})
export class ListActorsComponent implements OnInit {

  constructor(
    public apiresponse:ActorsService,
    private toastr: ToastrService
  ) { }

  ngOnInit(): void {
    this.apiresponse.get();
  }
  update(request: Actors){
    this.apiresponse.update(request);
  }
  delete(id?:number){
    if(confirm("Esta seguro que desea eliminar el registro")){
        this.apiresponse.delete(id).subscribe(response=>{
        if(response?.data){
          this.apiresponse.get();
        }
        else{
          this.toastr.error("Datos no eliminado","Actors");
        }
      });
    }
  }

}
