import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { Subscription } from 'rxjs';
import { Actors, ActorsRequest } from 'src/app/models/actors';
import { ActorsService } from 'src/app/services/actors/actors.service';
import { CountriesService } from 'src/app/services/countries/countries.service';

@Component({
  selector: 'app-form-actors',
  templateUrl: './form-actors.component.html',
  styleUrls: ['./form-actors.component.scss']
})
export class FormActorsComponent implements OnInit,OnDestroy {
  form!: FormGroup;
  subscription!: Subscription;
  data!: Actors;
  id: number | undefined;
  constructor(private formBuilder:FormBuilder,private _api: ActorsService,
    private toastr: ToastrService,public apiresponsec:CountriesService) { 
      this.form=this.formBuilder.group({
        firstName:['',[Validators.required,Validators.maxLength(50)]],
        lastName:['',[Validators.required,Validators.maxLength(50)]],
        idCountry:['',[Validators.required]],
      });
    }
  ngOnDestroy(): void {
    this.subscription?.unsubscribe();
  }

  ngOnInit(): void {
    this.subscription=this._api.getdata().subscribe(response=>{
      this.data=response as Actors;
      this.form.patchValue({
        firstName: this.data.firstName,
        lastName: this.data.lastName,
        idCountry:this.data.idCountry
      });
      this.id=this.data.id;
    });
    this.apiresponsec.get();
  }
  save(){
    if(this.id===undefined || this.id===0){
      this.insert();
    }else{
      this.updateform();
    }
  }

  insert(){
    const fvalue: ActorsRequest={
      firstName: this.form.get('firstName')?.value,
      lastName: this.form.get('lastName')?.value,
      idCountry: this.form.get('idCountry')?.value
    }
    this._api.save(fvalue).subscribe(response=> {
      if(typeof response?.data! == "object") {
        this.toastr.success("Registro agregado","Actors");
        this._api.get();
        this.form.reset();
      }
      else{
        if(typeof response?.errors! == "object") {
          Object.values(response?.errors).forEach((message : any) => {
            this.toastr.error(message,"Actors");
          });
        }
      }
    });
  }
  updateform(){
    const fvalue: Actors={
      id:this.data.id,
      firstName: this.form.get('firstName')?.value,
      lastName: this.form.get('lastName')?.value,
      idCountry: this.form.get('idCountry')?.value
    }
    this._api.updatedata(fvalue,this.data?.id).subscribe(response=>{
      if(response.data){
        this.toastr.info("Registro actualizado","Actors");
        this._api.get();
        this.form.reset();
        this.id=0;
      }
      else{
        if(typeof response?.errors! == "object") {
          Object.values(response?.errors).forEach((message : any) => {
            this.toastr.error(message,"Actors");
          });
        }
      }
    });
  }
}
