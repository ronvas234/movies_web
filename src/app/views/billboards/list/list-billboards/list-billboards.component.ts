import { Component, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { Billboards } from 'src/app/models/billboards';
import { BillboardsService } from 'src/app/services/billboards/billboards.service';

@Component({
  selector: 'app-list-billboards',
  templateUrl: './list-billboards.component.html',
  styleUrls: ['./list-billboards.component.scss']
})
export class ListBillboardsComponent implements OnInit {

  constructor(
    public apiresponse:BillboardsService,
    private toastr: ToastrService
  ) { }

  ngOnInit(): void {
    this.apiresponse.get();
  }
  update(request: Billboards){
    this.apiresponse.update(request);
  }
  delete(id?:number){
    if(confirm("Esta seguro que desea eliminar el registro")){
        this.apiresponse.delete(id).subscribe(response=>{
        if(response?.data){
          this.apiresponse.get();
        }
        else{
          this.toastr.error("Datos no eliminado","Billboards");
        }
      });
    }
  }

}
