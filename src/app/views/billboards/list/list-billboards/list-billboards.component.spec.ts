import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ListBillboardsComponent } from './list-billboards.component';

describe('ListBillboardsComponent', () => {
  let component: ListBillboardsComponent;
  let fixture: ComponentFixture<ListBillboardsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ListBillboardsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ListBillboardsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
