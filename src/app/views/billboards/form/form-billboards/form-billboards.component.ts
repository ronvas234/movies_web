import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { Subscription } from 'rxjs';
import { Billboards, BillboardsRequest } from 'src/app/models/billboards';
import { ActorsService } from 'src/app/services/actors/actors.service';
import { BillboardsService } from 'src/app/services/billboards/billboards.service';
import { CountriesService } from 'src/app/services/countries/countries.service';
import { DirectorsService } from 'src/app/services/directors/directors.service';
import { GendersService } from 'src/app/services/genders/genders.service';

@Component({
  selector: 'app-form-billboards',
  templateUrl: './form-billboards.component.html',
  styleUrls: ['./form-billboards.component.scss']
})
export class FormBillboardsComponent implements OnInit,OnDestroy {
  form!: FormGroup;
  subscription!: Subscription;
  data!: Billboards;
  id: number | undefined;
  constructor(private formBuilder:FormBuilder,private _api: BillboardsService,
    private toastr: ToastrService,public apiresponsec:CountriesService,
    public apiresponseg:GendersService,public apiresponsed:DirectorsService,
    public apiresponsea:ActorsService) { 
      this.form=this.formBuilder.group({
        titles:['',[Validators.required,Validators.maxLength(50)]],
        review:['',[Validators.required,Validators.maxLength(500)]],
        codeTrailer:['',[Validators.required,Validators.maxLength(30)]],
        images:['',[Validators.required,Validators.maxLength(250)]],
        idCountry:['',[Validators.required]],
        idGender:['',[Validators.required]],
        idActor:['',[Validators.required]],
        idDirector:['',[Validators.required]]
      });
    }
  ngOnDestroy(): void {
    this.subscription?.unsubscribe();
  }

  ngOnInit(): void {
    this.subscription=this._api.getdata().subscribe(response=>{
      this.data=response as Billboards;
      this.form.patchValue({
        titles: this.data.titles,
        review: this.data.review,
        codeTrailer: this.data.codeTrailer,
        images: this.data.images,
        idCountry:this.data.idCountry,
        idActor:this.data.idActor,
        idDirector:this.data.idDirector,
        idGender:this.data.idGender
      });
      this.id=this.data.id;
    });
    this.apiresponsec.get();
    this.apiresponsed.get();
    this.apiresponseg.get();
    this.apiresponsea.get();
  }
  save(){
    if(this.id===undefined || this.id===0){
      this.insert();
    }else{
      this.updateform();
    }
  }

  insert(){
    const fvalue: BillboardsRequest={
        titles: this.form.get('titles')?.value,
        review: this.form.get('review')?.value,
        codeTrailer: this.form.get('codeTrailer')?.value,
        images:this.form.get('images')?.value,
        idCountry:this.form.get('idCountry')?.value,
        idActor:this.form.get('idActor')?.value,
        idDirector:this.form.get('idDirector')?.value,
        idGender:this.form.get('idGender')?.value
    }
    this._api.save(fvalue).subscribe(response=> {
      if(typeof response?.data! == "object") {
        this.toastr.success("Registro agregado","Billboards");
        this._api.get();
        this.form.reset();
      }
      else{
        if(typeof response?.errors! == "object") {
          Object.values(response?.errors).forEach((message : any) => {
            this.toastr.error(message,"Billboards");
          });
        }
      }
    });
  }
  updateform(){
    const fvalue: Billboards={
      id:this.data.id,
      titles: this.form.get('titles')?.value,
      review: this.form.get('review')?.value,
      codeTrailer: this.form.get('codeTrailer')?.value,
      images:this.form.get('images')?.value,
      idCountry:this.form.get('idCountry')?.value,
      idActor:this.form.get('idActor')?.value,
      idDirector:this.form.get('idDirector')?.value,
      idGender:this.form.get('idGender')?.value
    }
    this._api.updatedata(fvalue,this.data?.id).subscribe(response=>{
      if(response.data){
        this.toastr.info("Registro actualizado","Billboards");
        this._api.get();
        this.form.reset();
        this.id=0;
      }
      else{
        if(typeof response?.errors! == "object") {
          Object.values(response?.errors).forEach((message : any) => {
            this.toastr.error(message,"Billboards");
          });
        }
      }
    });
  }
}
