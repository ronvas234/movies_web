import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FormBillboardsComponent } from './form-billboards.component';

describe('FormBillboardsComponent', () => {
  let component: FormBillboardsComponent;
  let fixture: ComponentFixture<FormBillboardsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FormBillboardsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FormBillboardsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
