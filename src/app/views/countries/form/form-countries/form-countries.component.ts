import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { Subscription } from 'rxjs';
import { Countries, CountriesRequest } from 'src/app/models/countries';
import { CountriesService } from 'src/app/services/countries/countries.service';

@Component({
  selector: 'app-form-countries',
  templateUrl: './form-countries.component.html',
  styleUrls: ['./form-countries.component.scss']
})
export class FormCountriesComponent implements OnInit,OnDestroy {
  form!: FormGroup;
  subscription!: Subscription;
  data!: Countries;
  id: number | undefined;
  constructor(private formBuilder:FormBuilder,private _api: CountriesService,
    private toastr: ToastrService) { 
      this.form=this.formBuilder.group({
        name:['',[Validators.required,Validators.maxLength(50)]],
      });
    }
  ngOnDestroy(): void {
    this.subscription?.unsubscribe();
  }

  ngOnInit(): void {
    this.subscription=this._api.getdata().subscribe(response=>{
      this.data=response as Countries;
      this.form.patchValue({
        name: this.data.name,
      });
      this.id=this.data.id;
    });
  }
  save(){
    if(this.id===undefined || this.id===0){
      this.insert();
    }else{
      this.updateform();
    }
  }

  insert(){
    const fvalue: CountriesRequest={
      name: this.form.get('name')?.value
    }
    this._api.save(fvalue).subscribe(response=> {
      if(typeof response?.data! == "object") {
        this.toastr.success("Registro agregado","Countries");
        this._api.get();
        this.form.reset();
      }
      else{
        if(typeof response?.errors! == "object") {
          Object.values(response?.errors).forEach((message : any) => {
            this.toastr.error(message,"Countries");
          });
        }
      }
    });
  }
  updateform(){
    const fvalue: Countries={
      id:this.data.id,
      name: this.form.get('name')?.value
    }
    this._api.updatedata(fvalue,this.data?.id).subscribe(response=>{
      if(response.data){
        this.toastr.info("Registro actualizado","Countries");
        this._api.get();
        this.form.reset();
        this.id=0;
      }
      else{
        if(typeof response?.errors! == "object") {
          Object.values(response?.errors).forEach((message : any) => {
            this.toastr.error(message,"Countries");
          });
        }
      }
    });
  }
}
