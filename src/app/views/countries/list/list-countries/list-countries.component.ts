import { Component, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { Countries, CountriesRequest } from 'src/app/models/countries';
import { CountriesService } from 'src/app/services/countries/countries.service';
import { Response } from 'src/app/models/response';

@Component({
  selector: 'app-list-countries',
  templateUrl: './list-countries.component.html',
  styleUrls: ['./list-countries.component.scss']
})
export class ListCountriesComponent implements OnInit {
  constructor(
    public apiresponse:CountriesService,
    private toastr: ToastrService
  ) { }

  ngOnInit(): void {
    this.apiresponse.get();
  }
  update(request: Countries){
    this.apiresponse.update(request);
  }
  delete(id?:number){
    if(confirm("Esta seguro que desea eliminar el registro")){
        this.apiresponse.delete(id).subscribe(response=>{
        if(response?.data){
          this.apiresponse.get();
        }
        else{
          this.toastr.error("Datos no eliminado","Countries");
        }
      });
    }
  }
}
