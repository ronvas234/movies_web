import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { Subscription } from 'rxjs';
import { Genders, GendersRequest } from 'src/app/models/genders';
import { GendersService } from 'src/app/services/genders/genders.service';

@Component({
  selector: 'app-form-genders',
  templateUrl: './form-genders.component.html',
  styleUrls: ['./form-genders.component.scss']
})
export class FormGendersComponent implements OnInit,OnDestroy {
  form!: FormGroup;
  subscription!: Subscription;
  data!: Genders;
  id: number | undefined;
  constructor(private formBuilder:FormBuilder,private _api: GendersService,
    private toastr: ToastrService) { 
      this.form=this.formBuilder.group({
        name:['',[Validators.required,Validators.maxLength(50)]],
      });
    }
  ngOnDestroy(): void {
    this.subscription?.unsubscribe();
  }

  ngOnInit(): void {
    this.subscription=this._api.getdata().subscribe(response=>{
      this.data=response as Genders;
      this.form.patchValue({
        name: this.data.name,
      });
      this.id=this.data.id;
    });
  }
  save(){
    if(this.id===undefined || this.id===0){
      this.insert();
    }else{
      this.updateform();
    }
  }

  insert(){
    const fvalue: GendersRequest={
      name: this.form.get('name')?.value
    }
    this._api.save(fvalue).subscribe(response=> {
      if(typeof response?.data! == "object") {
        this.toastr.success("Registro agregado","Genders");
        this._api.get();
        this.form.reset();
      }
      else{
        if(typeof response?.errors! == "object") {
          Object.values(response?.errors).forEach((message : any) => {
            this.toastr.error(message,"Genders");
          });
        }
      }
    });
  }
  updateform(){
    const fvalue: Genders={
      id:this.data.id,
      name: this.form.get('name')?.value
    }
    this._api.updatedata(fvalue,this.data?.id).subscribe(response=>{
      if(response.data){
        this.toastr.info("Registro actualizado","Genders");
        this._api.get();
        this.form.reset();
        this.id=0;
      }
      else{
        if(typeof response?.errors! == "object") {
          Object.values(response?.errors).forEach((message : any) => {
            this.toastr.error(message,"Genders");
          });
        }
      }
    });
  }
}
