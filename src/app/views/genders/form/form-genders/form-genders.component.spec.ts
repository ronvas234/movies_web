import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FormGendersComponent } from './form-genders.component';

describe('FormGendersComponent', () => {
  let component: FormGendersComponent;
  let fixture: ComponentFixture<FormGendersComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FormGendersComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FormGendersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
