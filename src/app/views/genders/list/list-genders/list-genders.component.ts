import { Component, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { Genders } from 'src/app/models/genders';
import { GendersService } from 'src/app/services/genders/genders.service';

@Component({
  selector: 'app-list-genders',
  templateUrl: './list-genders.component.html',
  styleUrls: ['./list-genders.component.scss']
})
export class ListGendersComponent implements OnInit {

  constructor(
    public apiresponse:GendersService,
    private toastr: ToastrService
  ) { }

  ngOnInit(): void {
    this.apiresponse.get();
  }
  update(request: Genders){
    this.apiresponse.update(request);
  }
  delete(id?:number){
    if(confirm("Esta seguro que desea eliminar el registro")){
        this.apiresponse.delete(id).subscribe(response=>{
        if(response?.data){
          this.apiresponse.get();
        }
        else{
          this.toastr.error("Datos no eliminado","Genders");
        }
      });
    }
  }
}
