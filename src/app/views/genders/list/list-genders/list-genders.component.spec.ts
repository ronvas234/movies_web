import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ListGendersComponent } from './list-genders.component';

describe('ListGendersComponent', () => {
  let component: ListGendersComponent;
  let fixture: ComponentFixture<ListGendersComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ListGendersComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ListGendersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
