import { Component, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { Directors } from 'src/app/models/directors';
import { DirectorsService } from 'src/app/services/directors/directors.service';

@Component({
  selector: 'app-list-directors',
  templateUrl: './list-directors.component.html',
  styleUrls: ['./list-directors.component.scss']
})
export class ListDirectorsComponent implements OnInit {

  constructor(
    public apiresponse:DirectorsService,
    private toastr: ToastrService
  ) { }

  ngOnInit(): void {
    this.apiresponse.get();
  }
  update(request: Directors){
    this.apiresponse.update(request);
  }
  delete(id?:number){
    if(confirm("Esta seguro que desea eliminar el registro")){
        this.apiresponse.delete(id).subscribe(response=>{
        if(response?.data){
          this.apiresponse.get();
        }
        else{
          this.toastr.error("Datos no eliminado","Directors");
        }
      });
    }
  }

}
