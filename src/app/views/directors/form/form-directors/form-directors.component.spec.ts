import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FormDirectorsComponent } from './form-directors.component';

describe('FormDirectorsComponent', () => {
  let component: FormDirectorsComponent;
  let fixture: ComponentFixture<FormDirectorsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FormDirectorsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FormDirectorsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
