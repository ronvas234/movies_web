import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ActorsComponent } from './views/actors/actors.component';
import { BillboardsComponent } from './views/billboards/billboards.component';
import { CountriesComponent } from './views/countries/countries.component';
import { DirectorsComponent } from './views/directors/directors.component';
import { GendersComponent } from './views/genders/genders.component';
import { HomeComponent } from './views/home/home.component';

const routes: Routes = [
  { path: '', component: HomeComponent,pathMatch: 'full' },
  { path: 'countries', component: CountriesComponent },
  { path: 'directors', component: DirectorsComponent },
  { path: 'genders', component: GendersComponent },
  { path: 'actors', component: ActorsComponent },
  { path: 'billboards', component: BillboardsComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
