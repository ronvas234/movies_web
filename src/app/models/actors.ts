import { BaseObject } from "./baseobject";

export class Actors extends BaseObject{
    firstName: string | undefined;
    lastName: string | undefined;
    idCountry: number | undefined;
  }
export class ActorsRequest{
    firstName: string | undefined;
    lastName: string | undefined;
    idCountry: number | undefined;
}