export interface Response{
    data: any,
    meta: any,
    errors: any
}