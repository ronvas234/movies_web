import { BaseObject } from "./baseobject";

export class Directors extends BaseObject{
    firstName: string | undefined;
    lastName: string | undefined;
    idCountry: number | undefined;
  }
export class DirectorsRequest{
    firstName: string | undefined;
    lastName: string | undefined;
    idCountry: number | undefined;
}