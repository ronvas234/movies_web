import { BaseObject } from "./baseobject";

export class Billboards extends BaseObject{
    titles: string | undefined;
    review: string | undefined;
    images: string | undefined;
    codeTrailer: string | undefined;
    idCountry: number | undefined;
    idGender: number | undefined;
    idActor: number | undefined;
    idDirector: number | undefined;
  }
export class BillboardsRequest{
    titles: string | undefined;
    review: string | undefined;
    images: string | undefined;
    codeTrailer: string | undefined;
    idCountry: number | undefined;
    idGender: number | undefined;
    idActor: number | undefined;
    idDirector: number | undefined;
}